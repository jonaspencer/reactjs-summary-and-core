import Todo from "./components/Todo";

function App() {
  return(
    <div>
      <h1> My List</h1>
      <Todo title='Learn React1'/>
      <Todo title='Learn React2'/>
      <Todo title='Learn React3'/>
    </div>
  );
}

export default App;
