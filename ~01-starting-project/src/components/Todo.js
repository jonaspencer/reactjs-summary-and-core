import { useState } from "react";
import Modal from "./Modal";
import Backdrop from "./Backdrop";

function Todo(props){
    const [modalOpen, setModalOpen] = useState(false);

    function deleteHandler(){
        setModalOpen(true);
    }

    function closeHandler(){
        setModalOpen(false);
    }

    return(
    <div className='card'>
        <h2>{props.title}</h2>
        <div className='actions'>
          <button className='btn' onClick={deleteHandler}>Delete</button>
        </div>
        {modalOpen && <Modal onCancel={closeHandler} onConfirm={closeHandler}/>}
        {modalOpen && <Backdrop onCancel={closeHandler}/>}
      </div>
    );
}
export default Todo;