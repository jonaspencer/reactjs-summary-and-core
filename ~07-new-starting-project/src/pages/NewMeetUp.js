import NewMeetupForm from "../components/meetups/NewMeetupForm";
import {useHistory} from 'react-router-dom';

//https://react-getting-started-880a0-default-rtdb.firebaseio.com/

function NewMeetUpPage(){
    const history = useHistory();

    function addMeetupHandler(meetupData) {
        fetch('https://react-getting-started-880a0-default-rtdb.firebaseio.com/meetups.json',
        {
            method: 'POST',
            body: JSON.stringify(meetupData),
            headers:{
                'Content-Type': 'application.json'
            }
        }).then(() => {
            history.replace('/');
        });
    };

    return (
        <section>
            <h1>Add new Meetup</h1>
            <NewMeetupForm onAddMeetup={addMeetupHandler}/>
        </section>    
    );
}

export default NewMeetUpPage;